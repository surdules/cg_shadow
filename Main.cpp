
#include <windows.h>

#include <GL/gl.h>
#include <GL/glut.h>

#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include <iostream>
#include <iomanip>
#include <sstream>

#include "Const.h"

#include "Transformer.h"
#include "TransformerArcBall.h"
#include "TransformerPan.h"
#include "TransformerZoom.h"

#include "Application.h"

#include "Painter.h"
#include "PainterStencil.h"
#include "PainterPixelShader.h"

/**
    @file Main.cpp
    @brief Implements the Shadow demonstration.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

using namespace std;

/** @brief Constant representind the default width of the application window. */
static const int K_WINDOW_WIDTH = 640;
/** @brief Constant representind the default height of the application window. */
static const int K_WINDOW_HEIGHT = 480;

/** @brief Constant representind the distance from the eye to the 'near' frustum plane. */
static const float K_FRUSTUM_Z_NEAR = 1.0f;
/** @brief Constant representind the distance from the eye to the 'far' frustum plane. */
static const float K_FRUSTUM_Z_FAR = 100.0f;
/** @brief Constant representind the angle of the field of view. */
static const float K_FRUSTUM_FOV = 90.0f;

/** @brief The dimensions of the Window which appears on the screen. */
static int s_windowWidth, s_windowHeight;

/** @brief The menu identifier for the application menu. */
static int s_menuHandle;

/** @brief Identifier for menu entry. */
static const int K_MENU_SCENE = 0;
/** @brief Identifier for menu entry. */
static const int K_MENU_MODEL = 1;
/** @brief Identifier for menu entry. */
static const int K_MENU_LIGHT = 2;

/** @brief The Cg context in which the pixel shader code executes. */
static CGcontext s_CgContext;

/** @brief The Cg profile in which the pixel shader code executes. */
static CGprofile s_CgProfile;

/** @brief The Cg pixel shader program. */
static CGprogram s_CgProgram;

/**
    @brief The current transformation matrix for the scene.
    
    This matrix is initialized when the user presses the mouse
    button (to start defining a new transformation) and is updated 
    when the user releases the mouse button (to end the current
    transformation).
*/
static Matrix s_transformationMatrix;

/**
    @brief The type of transformation matrix (Application#getSceneMatrix,
    Application#getModelMatrix, Application#getLightMatrix).
*/
static int s_transformationType;

/** @brief The Transformer which moves the objects on the screen. */
static Transformer* s_transformer;

/** @brief The Application that renders the scene. */
static Application* s_application;

/**
    @brief Displays a string at the given coordinates.

    Uses glutBitmapCharacter() to draw the string at 
    the specified coordinates with the font
    GLUT_BITMAP_HELVETICA_12
*/
void displayString(int x, int y, const char* message)
{
    while(*message) {
        glRasterPos2i(x, y);
        glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, *message);
        x = x + glutBitmapWidth(GLUT_BITMAP_HELVETICA_12, *message);
        message++;
    }
}

/**
    @brief Displays the current status (frames per second).

    This function should be called once per frame.
*/
void displayStatus()
{
    static int frames = 0;
    static int startTime = glutGet(GLUT_ELAPSED_TIME);

    frames++;

    double deltaTime = (glutGet(GLUT_ELAPSED_TIME) - startTime) / 1000.0;
    if (deltaTime > EPSILON) {
        glPushAttrib(GL_TRANSFORM_BIT | GL_LIGHTING_BIT);

        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluOrtho2D(0.0, (GLfloat) s_windowWidth, 0.0, (GLfloat) s_windowHeight);

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();

        glDisable(GL_LIGHTING);

        ostringstream status;
        status << "FPS:" << setprecision(3) << (frames / deltaTime);
        displayString(20, 20, status.str().c_str());

        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glPopMatrix();

        glPopAttrib();

        if (deltaTime > 1.0) {
            startTime = glutGet(GLUT_ELAPSED_TIME);
            frames = 0;
        }
    }
}

/**
    @brief Callback function invoked to render the contents of
    the GLUT window.

    This function is invoked from #motionCallback to render the 
    scene when a new transformation has been defined by the user.

    The scene is drawn by calling Application#draw
*/
void displayCallback()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    glLoadIdentity();

    const Vector& eye = s_application->getEyePosition();
    gluLookAt(
        eye[Vector::X], eye[Vector::Y], eye[Vector::Z],
        0.0, 0.0, 0.0,  // Center of the screen
        0.0, 1.0, 0.0); // Up direction

    s_application->draw();

    displayStatus();

    glutSwapBuffers();
}

/**
    @brief Callback function invoked when the window is resized.

    This function is invoked when the window first appears on the
    screen as well as everytime the window is resized.

    The function updates the Transformer dimensions and sets up the 
    perspective to use a specified FOV and Z range.
*/
void reshapeCallback(int width, int height)
{
    s_windowWidth = width;
    s_windowHeight = height;

    s_transformer->reshape(width, height);

    glViewport(0, 0,(GLsizei)width,(GLsizei)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(K_FRUSTUM_FOV, (GLfloat)width / (GLfloat)height, 
        K_FRUSTUM_Z_NEAR, K_FRUSTUM_Z_FAR);

    // Change the Projection matrix to have the far plane
    // at infinity.
    // In a standard Projection matrix P, the Near and Far
    // plane distances appear only in entry (2,2) as
    // -(Far+Near)/(Far-Near) and in entry (2,3) as
    // -2*Far*Near/(Far-Near). As Far goes to infinity,
    // these entries become -1 and, respectively, -2*Near
    GLfloat data[16];
    glGetFloatv(GL_PROJECTION_MATRIX, data);
    Matrix projection(data);

    projection(2, 2) = -1;
    projection(2, 3) = -2 * K_FRUSTUM_Z_NEAR;

    glLoadMatrixf(projection.toArray());

    glMatrixMode(GL_MODELVIEW);
}

/**
    @brief Update the Application matrix specified by 
    #s_transformationType with the contents of the current transformation 
    matrix.
*/
void setApplicationMatrix(Matrix& matrix)
{
    switch(s_transformationType) {
    case K_MENU_SCENE:
        s_application->setSceneMatrix(matrix);
        break;
    case K_MENU_MODEL:
        s_application->setModelMatrix(matrix);
        break;
    case K_MENU_LIGHT:
        s_application->setLightMatrix(matrix);
        break;
    default:
        cerr << "setApplicationMatrix(): unrecognized transformation type: " << s_transformationType << endl;
        exit(1);
    }
}

/**
    @brief Callback function invoked when a mouse button is pressed
    or released.

    The function ignores all but the left mouse button events. 

    If the left mouse button is pressed, the function starts a new
    transformation on the current Transformer.

    If the left mouse button is released, the function sets the
    transformation matrix on the Application.

    Since in GLUT coordinates the Y coordinate increases from top
    to bottom (which is backwards from the way OpenGL expects it)
    the y coordinate is flipped first.
*/
void mouseCallback(int button, int state, int x, int y)
{
    if (button != GLUT_LEFT_BUTTON) {
        return;
    }

    y = s_windowHeight - y;

    if (state == GLUT_DOWN) {
        delete s_transformer;

        switch(glutGetModifiers()) {
        case GLUT_ACTIVE_SHIFT:
            s_transformer = new TransformerZoom(s_windowWidth, s_windowHeight, 0.1f);
            break;
        case GLUT_ACTIVE_CTRL:
            s_transformer = new TransformerPan(s_windowWidth, s_windowHeight, 0.1f);
            break;
        default:
            s_transformer = new TransformerArcBall(s_windowWidth, s_windowHeight);
            break;
        }

        s_transformer->start(x, y);
    } else if (state == GLUT_UP) {
        Matrix matrix = s_transformer->end(x, y).multiplyRight(s_transformationMatrix);
        setApplicationMatrix(matrix);

        s_transformationMatrix = matrix;
    } else {
        cerr << "mouseCallback(): unrecognized mouse button state: " << state << endl;
        exit(1);
    }
}

/**
    @brief Callback function invoked when the mouse is moved while
    a button is pressed.

    Computes the transformation matrix at the current mouse position
    by calling Transformer#end.

    Since in GLUT coordinates the Y coordinate increases from top
    to bottom (which is backwards from the way OpenGL expects it)
    the y coordinate is flipped first.
*/
void motionCallback(int x, int y)
{
    y = s_windowHeight - y;

    Matrix matrix = s_transformer->end(x, y).multiplyRight(s_transformationMatrix);
    setApplicationMatrix(matrix);

    glutPostRedisplay();
}

/**
    @brief Callback function invoked when a menu entry is selected.

    Changes the transformation matrix and type based on the user
    selection.
*/
void menuCallback(int value)
{
    switch(value) {
    case K_MENU_SCENE:
        s_transformationType = K_MENU_SCENE;
        s_transformationMatrix = s_application->getSceneMatrix();
        break;
    case K_MENU_MODEL:
        s_transformationType = K_MENU_MODEL;
        s_transformationMatrix = s_application->getModelMatrix();
        break;
    case K_MENU_LIGHT:
        s_transformationType = K_MENU_LIGHT;
        s_transformationMatrix = s_application->getLightMatrix();
        break;
    default:
        cerr << "menuCallback(): unrecognized menu entry: " << value << endl;
        exit(1);
    }
}

/**
    @brief Called when the application is idle.

    Refreshes the screen (glutPostRedisplay()).
*/
void idleCallback()
{
    glutPostRedisplay();
}

/** 
    @brief Initialize GLUT. 

    Create the window, menu, and setup the appropriate callbacks.
*/
void initGLUT(int argc, char** argv)
{
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_STENCIL);

    glutInitWindowSize(K_WINDOW_WIDTH, K_WINDOW_HEIGHT); 
    glutInitWindowPosition(100, 100);

    glutCreateWindow(argv[0]);

    s_menuHandle = glutCreateMenu(menuCallback);
    glutSetMenu(s_menuHandle);

    glutAddMenuEntry("Control scene", K_MENU_SCENE);
    glutAddMenuEntry("Control model", K_MENU_MODEL);
    glutAddMenuEntry("Control light", K_MENU_LIGHT);

    glutAttachMenu(GLUT_RIGHT_BUTTON);

    glutDisplayFunc(displayCallback); 
    glutReshapeFunc(reshapeCallback);
    glutMouseFunc(mouseCallback);
    glutMotionFunc(motionCallback);
    glutIdleFunc(idleCallback);
}

/** @brief Initialize OpenGL. */
void initOpenGL()
{
    // Interpolate colors and normal vectors along
    // polygon edges
    glShadeModel(GL_SMOOTH);

    // Clear with black
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);

    // Enable depth testing
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // Enable lighting
    glEnable(GL_LIGHTING);

    // Nice perspective calculations (small performance hit.)
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
}

/** @brief Callback invoked when a Cg error is encountered. */
void cgErrorCallback()
{
    CGerror err = cgGetError();
    
    if (err != CG_NO_ERROR) {
        cerr << "cgErrorCallback(): " << cgGetErrorString(err) << endl;
        exit(1);
    }
}

/**
    @brief Initialize Cg.

    Setup the global Cg error callback (#cgErrorCallback), create the
    global Cg context (#s_CgContext), setup the Cg profile (#s_CgProfile)
    and load the Cg pixel shader program (#s_CgProgram).
*/
void initCg(const char* fileName)
{
    cgSetErrorCallback(cgErrorCallback);
    s_CgContext = cgCreateContext();

    s_CgProfile = cgGLGetLatestProfile(CG_GL_VERTEX);
    cgGLSetOptimalOptions(s_CgProfile);

    s_CgProgram = cgCreateProgramFromFile(
        s_CgContext,
        CG_SOURCE, 
        fileName,
        s_CgProfile,
        NULL,
        NULL);

    cgGLLoadProgram(s_CgProgram);
}

/**
    @brief The entry point into the application.
*/
int main(int argc, char** argv)
{
    if (argc < 3) {
        cerr << "main(): you must specify the model and pixel shader files" << endl;
        exit(1);
    }

    initGLUT(argc, argv);
    initOpenGL();
    initCg(argv[2]);

    s_application = new Application();
    s_application->init(argv[1]);

    //s_application->getModel().replacePainter(new PainterStencil(GL_LIGHT0, PainterStencil::Z_FAIL));
	s_application->getModel().replacePainter(new PainterPixelShader(s_CgProfile, s_CgProgram, GL_LIGHT0));

    s_transformer = new TransformerArcBall(K_WINDOW_WIDTH, K_WINDOW_HEIGHT);
    s_transformationType = K_MENU_SCENE;
    s_transformationMatrix = s_application->getSceneMatrix();

    glutMainLoop();

    delete s_application;

    cgDestroyProgram(s_CgProgram);
    cgDestroyContext(s_CgContext);

	return 0;
}
