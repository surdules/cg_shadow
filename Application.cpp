
#define WIN32_LEAN_AND_MEAN 
#include <windows.h>

#include <GL/gl.h>

#include "Application.h"

Application::Application() :
    m_lightAmbientColor(0.2f, 0.2f, 0.2f),
    m_lightDiffuseColor(1.0f, 1.0f, 1.0f),
    m_lightPosition(0.0f, 5.0f, 25.0f),
    m_eyePosition(0.0f, 0.0f, 30.0f),
    m_sceneMatrix(Matrix::IDENTITY),
    m_lightMatrix(Matrix::IDENTITY)
{
        // nop
}

void Application::init(const char* modelFileName)
{
    glLightfv(GL_LIGHT0, GL_AMBIENT, m_lightAmbientColor.toArray());
    glLightfv(GL_LIGHT0, GL_DIFFUSE, m_lightDiffuseColor.toArray());
    glLightfv(GL_LIGHT0, GL_POSITION, m_lightPosition.toArray());

    glEnable(GL_LIGHT0);

    m_model.loadModel(modelFileName, false);
	m_model.createEdgeTriangles();
}

const Vector& Application::getEyePosition() const
{
    return m_eyePosition;
}

const Matrix& Application::getSceneMatrix() const
{
    return m_sceneMatrix;
}

void Application::setSceneMatrix(const Matrix& sceneMatrix)
{
    m_sceneMatrix = sceneMatrix;
}

const Matrix& Application::getModelMatrix() const
{
    return m_model.getMatrix();
}

void Application::setModelMatrix(const Matrix& modelMatrix)
{
    m_model.setMatrix(modelMatrix);
}

const Matrix& Application::getLightMatrix() const
{
    return m_lightMatrix;
}

void Application::setLightMatrix(const Matrix& lightMatrix)
{
    m_lightMatrix = lightMatrix;
}

Model& Application::getModel() 
{
    return m_model;
}

void Application::drawScene() const
{
    glPushMatrix();
    glMultMatrixf(m_sceneMatrix.toArray());

    glNormal3f(0.0f, 0.0f, 1.0f);
    GLfloat color[4] = { 1.0f, 1.0f, 0.0f, 1.0f };
    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);

    glBegin(GL_QUADS);

    glVertex3f(-50.0f, -50.0f, -50.0f);
    glVertex3f( 50.0f, -50.0f, -50.0f);
    glVertex3f( 50.0f,  50.0f, -50.0f);
    glVertex3f(-50.0f,  50.0f, -50.0f);

    glEnd();

    glPopMatrix();
}

void Application::draw() const
{
    Vector light = m_lightPosition.multiplyLeft(m_lightMatrix);
    glLightfv(GL_LIGHT0, GL_POSITION, light.toArray());

    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT | GL_STENCIL_BUFFER_BIT);

    glDisable(GL_LIGHT0);

    drawScene();

    m_model.draw(m_eyePosition, light);

    glEnable(GL_LIGHT0);

    glDepthFunc(GL_EQUAL);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL, 0, ~0);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    drawScene();

    glPopAttrib();
}
