
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include "PainterStencil.h"

using namespace std;

PainterStencil::PainterStencil(int lightHandle, StencilTest test, bool show):
    m_lightHandle(lightHandle),
    m_stencilTest(test),
    m_showShadowVolume(show),
    m_extrusionFactor(100.0f)
{
    // nop
}

void PainterStencil::paintShadowVolumeCaps(const Model& model, const Vector& light) const
{
    glBegin(GL_TRIANGLES);

    const vector<Triangle*>& triangles = model.getTriangles();
    for (vector<Triangle*>::const_iterator iterator = triangles.begin(); 
        iterator != triangles.end();
        iterator++) {
        
        const Triangle* triangle = *iterator;

        for (int i = 0; i < 3; i++) {
            const Vector& vTriangle = triangle->getVertex(i).getLocation();
            if (triangle->isFacing(light)) {
                glVertex3fv(vTriangle.toArray());
            } else {
                Vector vExtrude = vTriangle.add(vTriangle.subtract(light).scale(m_extrusionFactor));
                // Place the extruded vector at infinity (homogenous coordinate = 0)
                vExtrude[Vector::W] = 0;
                glVertex4fv(vExtrude.toArray());
            }
        }
    }

    glEnd();
}

void PainterStencil::paintShadowVolumeSides(const Model& model, const Vector& light) const
{
    glBegin(GL_TRIANGLES);

    const vector<Triangle*>& triangles = model.getTriangles();
    for (vector<Triangle*>::const_iterator iterator = triangles.begin(); 
        iterator != triangles.end();
        iterator++) {
        
        const Triangle* triangle = *iterator;
        
        if (!triangle->isFacing(light)) {
            continue;
        }

        const map<int, const Triangle*>& neighbors = model.getNeighbors(triangle);

        for (int i = 0; i < 3; i++) {
            int edge = triangle->getEdgeIndex(i);
            if (neighbors.find(edge) != neighbors.end()) {
                const Triangle* neighbor = neighbors.find(edge)->second;
                if (neighbor->isFacing(light)) {
                    continue;
                }
            }
            
            const Vector& vTriangle1 = triangle->getVertex(i).getLocation();
            const Vector& vTriangle2 = triangle->getVertex((i + 1) % 3).getLocation();
            
            Vector vExtrude1 = vTriangle1.add(vTriangle1.subtract(light).scale(m_extrusionFactor));
            // Place the extruded vector at infinity (homogenous coordinate = 0)
            vExtrude1[Vector::W] = 0;

            Vector vExtrude2 = vTriangle2.add(vTriangle2.subtract(light).scale(m_extrusionFactor));
            // Place the extruded vector at infinity (homogenous coordinate = 0)
            vExtrude2[Vector::W] = 0;

            glVertex3fv(vTriangle2.toArray());
            glVertex3fv(vTriangle1.toArray());
            glVertex4fv(vExtrude1.toArray());

            glVertex4fv(vExtrude1.toArray());
            glVertex4fv(vExtrude2.toArray());
            glVertex3fv(vTriangle2.toArray());
        }
    }

    glEnd();
}

void PainterStencil::paintShadowVolume(const Model& model, const Vector& light) const
{
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_POLYGON_BIT | GL_STENCIL_BUFFER_BIT);

    if (m_showShadowVolume) {
        GLfloat color[4] = { 0.0f, 1.0f, 1.0f, 0.2f };
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, color);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    } else {
        glColorMask(0, 0, 0, 0);
    }

    glDepthMask(0);
    glEnable(GL_CULL_FACE);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_ALWAYS, 0, ~0);

    if (m_stencilTest == Z_FAIL) {
        glStencilOp(GL_KEEP, GL_INCR, GL_KEEP);
        glCullFace(GL_FRONT);

        paintShadowVolumeSides(model, light);
        paintShadowVolumeCaps(model, light);

        glStencilOp(GL_KEEP, GL_DECR, GL_KEEP);
        glCullFace(GL_BACK);

        paintShadowVolumeSides(model, light);
        paintShadowVolumeCaps(model, light);
    } else if (m_stencilTest == Z_PASS) {
        glStencilOp(GL_KEEP, GL_KEEP, GL_INCR);
        glCullFace(GL_BACK);

        paintShadowVolumeSides(model, light);

        glStencilOp(GL_KEEP, GL_KEEP, GL_DECR);
        glCullFace(GL_FRONT);

        paintShadowVolumeSides(model, light);
    }

    glPopAttrib();
}

void PainterStencil::paintModel(const Model& model, const Vector& eye, const Vector& light) const
{
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT | GL_STENCIL_BUFFER_BIT);

    glDisable(m_lightHandle);

    Painter::paintModel(model, eye, light);

    glEnable(m_lightHandle);

    paintShadowVolume(model, light);

    glDepthFunc(GL_EQUAL);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL, 0, ~0);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    Painter::paintModel(model, eye, light);

    glPopAttrib();
}
