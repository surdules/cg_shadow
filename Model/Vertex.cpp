
#include "Vertex.h"

Vertex::Vertex(const MS3DVertex* const vertex) :
    m_boneID(vertex->m_boneID),
    m_location(
        vertex->m_vertex[Vector::X],
        vertex->m_vertex[Vector::Y],
        vertex->m_vertex[Vector::Z])
{
    // nop
}

const Vector& Vertex::getLocation() const {
    return m_location;
}
