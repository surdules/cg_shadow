
#ifndef __PAINTERSTENCIL_H
#define __PAINTERSTENCIL_H

#include "Vector.h"

#include "Mesh.h"
#include "Triangle.h"

#include "Painter.h"

class PainterStencil: public Painter {
public:
    /** 
        @brief The type of stencil test to perform when
        rendering the shadow volume for a Model.
    */
    enum StencilTest { Z_FAIL, Z_PASS };

private:
    int m_lightHandle;
    StencilTest m_stencilTest;

    bool m_showShadowVolume;
    float m_extrusionFactor;

    /** 
        @brief Draw the top and bottom of the shadow volume of this Model.

        The top of the shadow volume is defined as all the Triangles which
        face (Triangle#isFacing) the light.

        The top of the shadow volume is defined as all the Triangles which
        face away from (Triangle#isFacing) the light.
    */
    void paintShadowVolumeCaps(const Model& model, const Vector& light) const;

    /** 
        @brief Draw the sides of the shadow volume of this Model.

        A side of the shadow volume is determined as follows:

        <ul>
            <li>For every Triangle which is visibile (Triangle#isFacing)
            from the light, determine if it has a neighbor which is
            not visible from the light.</li>
            <li>If such a neighbor is found, then the edge shared with
            this neighbor is a silhouette edge.</li>
            <li>Create a new edge by extending the silhouette edge along 
            the direction determined by the light and the two ends of 
            the edge.</li>
            <li>The original silhouette edge and the new extended edge
            create a shadow volume side quadrilateral.</li>
        </ul>
    */
    void paintShadowVolumeSides(const Model& model, const Vector& light) const;

    /** 
        @brief Draw the shadow volume of the Model into the stencil 
        buffer using the current #m_stencilTest depth test. If 
		#m_showShadowVolume is true, draw the shadow volume into the 
		color buffer as well (for debugging purposes).

        The Z_PASS algorithm is as follows:

        <ul>
            <li>The shadow volume is drawn uncapped (draw
            only the sides, not the top and the bottom); see
            PainterShadowStencil#paintShadowVolumeSides and
            PainterShadowStencil#paintShadowVolumeCaps).</li>
            <li>Do not update the color or the depth buffers.</li>
            <li>For every front-facing face of the shadow volume,
            increment the stencil buffer value whenever
            the Z test passes.</li>
            <li>For every back-facing face of the shadow volume,
            decrement the stencil buffer whenever the Z test
            passes.</li>
            <li>At the end, the stencil buffer will contain a
            non-zero value for every pixel that is in shadow.</li>
        </ul>

        The Z_FAIL algorithm is as follows:

        <ul>
            <li>The shadow volume is drawn capped (draw
            both the sides and the top and the bottom); see
            PainterShadowStencil#paintShadowVolumeSides and
            PainterShadowStencil#paintShadowVolumeCaps).</li>
            <li>Do not update the color or the depth buffers.</li>
            <li>For every back-facing face of the shadow volume,
            increment the stencil buffer value whenever
            the Z test fails.</li>
            <li>For every front-facing face of the shadow volume,
            decrement the stencil buffer whenever the Z test
            fails.</li>
            <li>At the end, the stencil buffer will contain a
            non-zero value for every pixel that is in shadow.</li>
        </ul>

        The Z_FAIL algorithm is non-intuitive and more robust than 
        the Z_PASS algorithm. A few observations:

        <ol>
            <li>The Z_PASS algorithm fails whenever a front-facing
            face of the shadow volume is clipped by the near plane.
            This can happen often and means that areas which are
            otherwise in the shadow appear in the light (the clipped
            portion of the front-facing face of the shadow volume
            does not increment the stencil value).</li>
            <li>The Z_FAIL algorithm is easier to understand if you
            draw a simple example on paper and work it out.</li>
            <li>The reason it is necessary to cap the shadow volume
            on both ends is simple to understand if you think of
            the shadow volume as being drawn from the back of the
            depth buffer towards the front. The bottom cap of the
            shadow volume is necessary to complete the shadow; the
            top cap of the shadow volume is necessary in order to
            prevent the visible faces of the model from appearing
            shadowed.</li>
        </ol>
    */
    void paintShadowVolume(const Model& model, const Vector& light) const;

public:
    PainterStencil(int lightHandle, StencilTest test = Z_PASS, bool show = false);

    virtual void paintModel(const Model& model, const Vector& eye, const Vector& light) const;
};

#endif // __PAINTERSTENCIL_H
