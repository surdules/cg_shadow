
#ifndef __PAINTERPIXELSHADER_H
#define __PAINTERPIXELSHADER_H

#include <Cg/cg.h>
#include <Cg/cgGL.h>

#include "Vector.h"

#include "Mesh.h"
#include "Triangle.h"

#include "Painter.h"

class PainterPixelShader: public Painter {
private:
    /** The CgProfile in which the #m_CgProgram will execute. */
    const CGprofile& m_CgProfile;
    /** The CgProgram which renders the fragments. */
    const CGprogram& m_CgProgram;

	int m_lightHandle;

	mutable bool m_extrude;
    float m_extrusionFactor;

    /** 
        @brief Draw the shadow volume of the Model into the stencil 
        buffer using the current #m_test depth test. If #m_show
        is true, draw the shadow volume into the color buffer as 
        well (for debugging purposes).

        The Z_PASS algorithm is as follows:

        <ul>
            <li>The shadow volume is drawn uncapped (draw
            only the sides, not the top and the bottom); see
            PainterShadowStencil#paintShadowVolumeSides and
            PainterShadowStencil#paintShadowVolumeCaps).</li>
            <li>Do not update the color or the depth buffers.</li>
            <li>For every front-facing face of the shadow volume,
            increment the stencil buffer value whenever
            the Z test passes.</li>
            <li>For every back-facing face of the shadow volume,
            decrement the stencil buffer whenever the Z test
            passes.</li>
            <li>At the end, the stencil buffer will contain a
            non-zero value for every pixel that is in shadow.</li>
        </ul>

        The Z_FAIL algorithm is as follows:

        <ul>
            <li>The shadow volume is drawn capped (draw
            both the sides and the top and the bottom); see
            PainterShadowStencil#paintShadowVolumeSides and
            PainterShadowStencil#paintShadowVolumeCaps).</li>
            <li>Do not update the color or the depth buffers.</li>
            <li>For every back-facing face of the shadow volume,
            increment the stencil buffer value whenever
            the Z test fails.</li>
            <li>For every front-facing face of the shadow volume,
            decrement the stencil buffer whenever the Z test
            fails.</li>
            <li>At the end, the stencil buffer will contain a
            non-zero value for every pixel that is in shadow.</li>
        </ul>

        The Z_FAIL algorithm is non-intuitive and more robust than 
        the Z_PASS algorithm. A few observations:

        <ol>
            <li>The Z_PASS algorithm fails whenever a front-facing
            face of the shadow volume is clipped by the near plane.
            This can happen often and means that areas which are
            otherwise in the shadow appear in the light (the clipped
            portion of the front-facing face of the shadow volume
            does not increment the stencil value).</li>
            <li>The Z_FAIL algorithm is easier to understand if you
            draw a simple example on paper and work it out.</li>
            <li>The reason it is necessary to cap the shadow volume
            on both ends is simple to understand if you think of
            the shadow volume as being drawn from the back of the
            depth buffer towards the front. The bottom cap of the
            shadow volume is necessary to complete the shadow; the
            top cap of the shadow volume is necessary in order to
            prevent the visible faces of the model from appearing
            shadowed.</li>
        </ol>
    */
    void paintShadowVolume(const Model& model, const Vector& eye, const Vector& light) const;

public:
    PainterPixelShader(const CGprofile& cgProfile, const CGprogram& cgProgram, int lightHandle);

    virtual void paintModel(const Model& model, const Vector& eye, const Vector& light) const;

    virtual void paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const;
};

#endif // __PAINTERPIXELSHADER_H
