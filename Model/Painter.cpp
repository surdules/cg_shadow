
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include "Painter.h"

using namespace std;

void Painter::paintModel(const Model& model, const Vector& eye, const Vector& light) const {
    const vector<Mesh*>& meshes = model.getMeshes();
    for (vector<Mesh*>::const_iterator iter = meshes.begin();
        iter != meshes.end();
        iter++) {
        paintMesh(*(*iter), eye, light);
    }
}

void Painter::paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const {
    if (!mesh.hasMaterial()) {
        return;
    }

    const Material& material = mesh.getMaterial();

    glPushAttrib(GL_TEXTURE_BIT);

    glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, material.getAmbientColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, material.getDiffuseColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, material.getSpecularColor().toArray());
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, material.getEmissiveColor().toArray());
    glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, material.getSpecularShininess());

    if (material.hasDetail()) {
        glBindTexture(GL_TEXTURE_2D, material.getDetailHandle());
        glEnable(GL_TEXTURE_2D);
    }

    const vector<const Triangle*>& triangles = mesh.getTriangles();
    for (vector<const Triangle*>::const_iterator iter = triangles.begin();
        iter != triangles.end();
        iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }

    glPopAttrib();
}

void Painter::paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const {
    glBegin(GL_TRIANGLES);

    for (int i = 0; i < 3; i++) {
        const Vector& normal = triangle.getNormal(i);
        glNormal3fv(normal.toArray());

        const Vector& texture = triangle.getTextureCoordinates(i);
        glTexCoord2f(texture[Vector::X], texture[Vector::Y]);

        const Vector& location = triangle.getVertex(i).getLocation();
        glVertex3fv(location.toArray());
    }

    glEnd();
}
