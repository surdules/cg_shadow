
#ifndef __MODEL_H
#define __MODEL_H

#pragma warning(disable: 4786)

#include <ostream>
#include <vector>
#include <map>

#include "Matrix.h"

#include "Material.h"

#include "Vertex.h"
#include "Triangle.h"
#include "Mesh.h"

class Painter;

/**
    @file Model.h
    @brief Declares the Model class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a 
    <a href="http://www.swissquake.ch/chumbalum-soft/ms3d/">Milkshape3D</a> Model.
*/
class Model {
private:
    /** @brief The list of Meshes that comprise this Model.*/
    std::vector<Mesh*> m_meshes;
    /** @brief The list of Triangles that comprise this Model.*/
    std::vector<Triangle*> m_triangles;
    /** @brief The list of Vertices that comprise this Model.*/
    std::vector<Vertex*> m_vertices;
    /** @brief The list of Materials that comprise this Model.*/
    std::vector<Material*> m_materials;

    /** 
        @brief The Triangle neighbor map for this Model.

        This map takes a Triangle and maps it to another map.
        The second map takes an edge of the original Triangle
        and maps it to the neighboring Triangle which shares
        that edge.

        The edge is an integer defined as described in
        Triangle#getEdgeIndex
    */
    std::map<const Triangle*, std::map<int, const Triangle*> > m_neighbors;

    /** @brief The Matrix which transforms this Model. */
    Matrix m_matrix;

    /** @brief The Painter which is used to #draw this Model. */
    Painter* m_painter;

private:
    /** @brief Do not allow this Model to be copied. */
    Model(const Model& copy);
    /** @brief Do not allow this Model to be copied. */
    Model& operator=(const Model& rhs);

    /** @brief Setup the #m_neighbors neighbor map. */
    void setupNeighborMap();

public:
    /** @brief Initialize the Model. */
    Model();
    /** 
        @brief Destroy the Model (free all Meshes, Triangles, Vertices
        and Materials.
    */
    ~Model();
    
    /** @brief Load the Model from the given Milkshape3D file. */
    void loadModel(const char* fileName, bool smooth = true);

    /** @brief Retrieve the list of Meshes comprising this Model. */
    const std::vector<Mesh*>& getMeshes() const;

    /** @brief Retrieve a specific Mesh from this Model. */
    const Mesh& getMesh(unsigned int index) const;

    /** @brief Retrieve the list of Triangles comprising this Model. */
    const std::vector<Triangle*>& getTriangles() const;

    /** @brief Retrieve a specific Triangle from this Model. */
    const Triangle& getTriangle(unsigned int index) const;

    /** @brief Retrieve the list of neighbors for a specific Triangle. */
    const std::map<int, const Triangle*>& getNeighbors(const Triangle* triangle) const;

	/** @brief Retrieve a specific Vertex from this Model. */
    const Vertex& getVertex(unsigned int index) const;

    /** @brief Retrieve a specific Material from this Model. */
    const Material& getMaterial(unsigned int index) const;

    /** @brief Retrieve the Matrix modifying this Model. */
    const Matrix& getMatrix() const;
    /** @brief Set the Matrix modifying this Model. */
    void setMatrix(const Matrix& matrix);

    /** 
        @brief Free the existing Painter used to draw this Model (if any) and
        replace it with a new one.
    */
    void replacePainter(Painter* painter);

	/** 
		@brief Create two triangles for every edge of every mesh in order to 
		render the shadow volume faces in a Cg vertex shader.
	*/
	void createEdgeTriangles();

    /**
        @brief Draw this model using the current #m_painter.
    */
    void draw(const Vector& eye, const Vector& light) const;

    /**
        @brief Insertion operator for this Model into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Model& model);
};

#endif // __MODEL_H
