
#ifndef __PAINTER_H
#define __PAINTER_H

#include "Vector.h"

#include "Triangle.h"
#include "Mesh.h"
#include "Model.h"

/**
    @file Painter.h
    @brief Declares the Painter class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a Painter visitor used to draw a Model and its constituents
    on the screen.
*/
class Painter {
public:
    /** 
        @brief Virtual (emtpy) destructor defined to ensure that derived classes' 
        destructors are invoked.
    */
    virtual ~Painter() {
        // nop
    }

    /** @brief Iterate through all the Meshes of the given Model and #paintMesh them. */
    virtual void paintModel(const Model& model, const Vector& eye, const Vector& light) const;
    /** 
        @brief Iterate through all the Triangles of the given Mesh and #paintTriangle them. 

        The rendering is "flat", using the material color and detail texture (if any).
    */
    virtual void paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const;
    /** 
        @brief Iterate through all the Vertices of the given Triangle and render them.

        The rendering is "flat", using the material color and detail texture (if any).
    */
    virtual void paintTriangle(const Triangle& triangle, const Vector& eye, const Vector& light) const;
};

#endif // __PAINTER_H
