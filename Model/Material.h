
#ifndef __MATERIAL_H
#define __MATERIAL_H

#include <string>

#include "MS3D.h"

#include "Arrays.h"

#include "Color.h"

/**
    @file Material.h
    @brief Declares the Material class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a Material used in a 
    <a href="http://www.swissquake.ch/chumbalum-soft/ms3d/">Milkshape3D</a> Model.
*/
class Material {
private:
    /** @brief The ambient color of this Material. */
    Color m_ambient;
    /** @brief The diffuse color of this Material. */
    Color m_diffuse;
    /** @brief The specular color of this Material. */
    Color m_specular;
    /** @brief The emissive color of this Material. */
    Color m_emissive;
    /** @brief The specular factor of this Material. */
    float m_shininess;
    /** @brief The name of the file containing the detail texture. */
    std::string m_detailFileName;
    /** @brief The name of the file containing the bumpmap texture. */
    std::string m_bumpmapFileName;

    /** @brief Does this material have a detail texture? */
    bool m_hasDetail;
    /** @brief Does this material have a bumpmap texture? */
    bool m_hasBumpmap;

    /** @brief The handle to the detail texture. */
    unsigned int m_detailHandle;

    /** 
        @brief The handle to the bumpmap (heightmap) texture.

        Each RGB element in this texture represents the height
        of the corresponding texel in the detail texture.

        The texture is generated with 50% luminance (divides all R, G
        and B components by two) because, when the bumpmap textures
        are combined with the detail texture, the blending equation
        multiplies everything by two.
    */
    unsigned int m_bumpmapHandle;

    /** 
        @brief The handle to the inverted bumpmap (heightmap) texture. 

        Each RGB element in this texture represents the negative height
        of the corresponding texel in the bumpmap texture. The negative
        height is computed by subtracting the bumpmap height from 255.

        The texture is generated with 50% luminance (divides all R, G
        and B components by two) because, when the bumpmap textures
        are combined with the detail texture, the blending equation
        multiplies everything by two.
    */
    unsigned int m_invertedBumpmapHandle;

    /** 
        @brief The handle to the normalmap texture.

        Each RGB element in the normalmap texture corresponds
        to the perturbed normal vector extracted from a heightmap.
        The perturbed normal is computed by computing the cross
        product of two tangent vectors at each entry in the
        heightmap.

        The tangent vectors are computed as:

        <pre>
        Vector s(1, 0, strength * (bumpmap(i + 1, j, 0) - bumpmap(i - 1, j, 0)));
        Vector t(0, 1, strength * (bumpmap(i, j + 1, 0) - bumpmap(i, j - 1, 0)));
        </pre>

        where strength is a scaling factor (default 0.01).

        The normal vector is the cross product of the two tangent
        vectors above, normalized (Vector#normalize), range 
        compressed (Vector#rangeCompress) and scaled by 255 in
        order to fill a byte.
    */
    unsigned int m_normalmapHandle;

private:
    /** @brief Do not allow this Material to be copied. */
    Material(const Material& copy);
    /** @brief Do not allow this Material to be copied. */
    Material& operator=(const Material& rhs);

    /** @brief Initialize the #m_detailHandle texture. */
    void initializeDetail(const Array3<unsigned char>& detailData);
    /** @brief Initialize the #m_bumpmapHandle texture. */
    void initializeBumpmap(const Array3<unsigned char>& bumpmapData);
    /** @brief Initialize the #m_invertedBumpmapHandle texture. */
    void initializeNormalmap(const Array3<unsigned char>& bumpmapData);

public:
    /** 
        @brief Initialize the Material using data from the corresponding
        Milkshape material.
    */
    Material(const MS3DMaterial* material);

    /** @brief Accessor for the ambient color. */
    const Color& getAmbientColor() const;
    /** @brief Accessor for the diffuse color. */
    const Color& getDiffuseColor() const;
    /** @brief Accessor for the specular color. */
    const Color& getSpecularColor() const;
    /** @brief Accessor for the emissive color. */
    const Color& getEmissiveColor() const;
    /** @brief Accessor for the specular factor. */
    float getSpecularShininess() const;

    /** @brief Does this Material have a detail texture? */
    bool hasDetail() const;
    /** @brief Does this Material have a bumpmap texture? */
    bool hasBumpmap() const;

    /** @brief Accessor for the Material detail texture handle. */
    unsigned int getDetailHandle() const;
    /** @brief Accessor for the Material bumpmap texture handle. */
    unsigned int getBumpmapHandle() const;
    /** @brief Accessor for the Material inverted bumpmap texture handle. */
    unsigned int getInvertedBumpmapHandle() const;
    /** @brief Accessor for the Material normalmap texture handle. */
    unsigned int getNormalmapHandle() const;
};

#endif // __MATERIAL_H
