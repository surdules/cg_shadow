
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <GL/gl.h>

#include "PainterPixelShader.h"

using namespace std;

PainterPixelShader::PainterPixelShader(const CGprofile& cgProfile, const CGprogram& cgProgram, int lightHandle):
    m_CgProfile(cgProfile),
	m_CgProgram(cgProgram),
    m_lightHandle(lightHandle),
	m_extrude(false),
    m_extrusionFactor(100.0f)
{
    // nop
}

void PainterPixelShader::paintShadowVolume(const Model& model, const Vector& eye, const Vector& light) const
{
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_POLYGON_BIT | GL_STENCIL_BUFFER_BIT);

    glColorMask(0, 0, 0, 0);

    glDepthMask(0);
    glEnable(GL_CULL_FACE);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_ALWAYS, 0, ~0);

	m_extrude = true;

    glStencilOp(GL_KEEP, GL_INCR, GL_KEEP);
    glCullFace(GL_FRONT);

	Painter::paintModel(model, eye, light);

    glStencilOp(GL_KEEP, GL_DECR, GL_KEEP);
    glCullFace(GL_BACK);

	Painter::paintModel(model, eye, light);

	m_extrude = false;

    glPopAttrib();
}

void PainterPixelShader::paintModel(const Model& model, const Vector& eye, const Vector& light) const
{
    glPushAttrib(GL_DEPTH_BUFFER_BIT | GL_LIGHTING_BIT | GL_STENCIL_BUFFER_BIT);

    // Use the Cg fragment program to compute the vertices
    cgGLBindProgram(m_CgProgram);
    cgGLEnableProfile(m_CgProfile);

    // Set the (fixed) model-view projection matrix
    cgGLSetStateMatrixParameter(cgGetNamedParameter(m_CgProgram, "modelViewProj"),
                                CG_GL_MODELVIEW_PROJECTION_MATRIX,
                                CG_GL_MATRIX_IDENTITY);

    // Set the (fixed) light position and colors
    cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "lightPosition"), light.toArray());

	GLfloat lightAmbientColor[4];
	glGetLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbientColor);
    cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "lightAmbientColor"), lightAmbientColor);

	GLfloat lightDiffuseColor[4];
	glGetLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuseColor);
    cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "lightDiffuseColor"), lightDiffuseColor);

	// Set the (fixed) eye position
    cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "eyePosition"), eye.toArray());

    glDisable(m_lightHandle);

    Painter::paintModel(model, eye, light);

    glEnable(m_lightHandle);

    paintShadowVolume(model, eye, light);

    glDepthFunc(GL_EQUAL);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_EQUAL, 0, ~0);
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

    Painter::paintModel(model, eye, light);

    cgGLDisableProfile(m_CgProfile);

	glPopAttrib();
}

void PainterPixelShader::paintMesh(const Mesh& mesh, const Vector& eye, const Vector& light) const {
    if (!mesh.hasMaterial()) {
        return;
    }

    const Material& material = mesh.getMaterial();

    glPushAttrib(GL_TEXTURE_BIT);

    cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "ambientColor"), 
		material.getAmbientColor().toArray());

	GLboolean lightEnabled[1];
	glGetBooleanv(GL_LIGHT0, lightEnabled);

	if (lightEnabled[0]) {
		cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "diffuseColor"), 
			material.getDiffuseColor().toArray());

		cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "emissiveColor"), 
			material.getEmissiveColor().toArray());

		cgGLSetParameter1f(cgGetNamedParameter(m_CgProgram, "specularShininess"),
			material.getSpecularShininess());
	} else {
		float zero[4] = { 0, 0, 0, 0 };

		cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "diffuseColor"), zero);

		cgGLSetParameter4fv(cgGetNamedParameter(m_CgProgram, "emissiveColor"), zero);

		cgGLSetParameter1f(cgGetNamedParameter(m_CgProgram, "specularShininess"), 0);
	}

    if (material.hasDetail()) {
		// TODO: use Cg sampler parameter for the texture
		/*
        glBindTexture(GL_TEXTURE_2D, material.getDetailHandle());
        glEnable(GL_TEXTURE_2D);
		*/
    }

	if (m_extrude) {
		cgGLSetParameter1f(cgGetNamedParameter(m_CgProgram, "extrusionFactor"), m_extrusionFactor);
	} else {
		cgGLSetParameter1f(cgGetNamedParameter(m_CgProgram, "extrusionFactor"), 0);
	}

	const vector<const Triangle*>& triangles = mesh.getTriangles();
    for (vector<const Triangle*>::const_iterator iter = triangles.begin();
        iter != triangles.end();
        iter++) {
        const Triangle* triangle = *iter;
        paintTriangle(*triangle, eye, light);
    }

    glPopAttrib();
}
