
#ifndef __MESH_H
#define __MESH_H

#include <ostream>
#include <vector>

#include "MS3D.h"

#include "Vector.h"

#include "Material.h"

#include "Triangle.h"

class Model;

/**
    @file Mesh.h
    @brief Declares the Mesh class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** 
    @brief Defines a Mesh used in a 
    <a href="http://www.swissquake.ch/chumbalum-soft/ms3d/">Milkshape3D</a> Model.
*/
class Mesh {
private:
    /** @brief The index of the Material used to draw the Triangles in this Mesh. */
    int m_materialIndex;
    /** @brief The Triangles representing this Mesh. */
    std::vector<const Triangle*> m_triangles;

    /** @brief The Model to which this Mesh belongs. */
    const Model& m_model;
    
private:
    /** @brief Do not allow this Mesh to be copied. */
    Mesh(const Mesh& copy);
    /** @brief Do not allow this Mesh to be copied. */
    Mesh& operator=(const Mesh& rhs);

public:
    /** @brief Create a new empty Mesh belonging to the specified Model. */
    Mesh(const Model& model);

    /** 
        @brief Initialize the Mesh using the (raw) data from the Milkshape
        model.

        The method reads the material index, vertex count and vertex indices
        and returns the (raw) pointer past the end of the mesh.
    */
    const char* initialize(const char* ptr);

    /** @brief Accessor for the Model of this Mesh. */
    const Model& getModel() const;

    /** @brief Does this Mesh have a material? */
    bool hasMaterial() const;
    /** @brief If Mesh#hasMaterial, return the Material. */
    const Material& getMaterial() const;
    /** @brief The Triangles for the faces of this Mesh. */
    const std::vector<const Triangle*>& getTriangles() const;
	/** @brief Add a new Triangle to this Mesh. */
	void addTriangle(const Triangle* triangle);

    /**
        @brief Insertion operator for this Mesh into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Mesh& mesh);
};

#endif // __MESH_H
