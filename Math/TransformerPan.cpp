
#include <assert.h>

#include "TransformerPan.h"

TransformerPan::TransformerPan(int width, int height, float scale) {
    reshape(width, height);
    m_scale = scale;
}

void TransformerPan::reshape(int width, int height) {
    assert((width > 0) && (height > 0));
}

void TransformerPan::start(float x, float y) {
    m_startX = x;
    m_startY = y;
}

const Matrix TransformerPan::end(float x, float y) {
    return Matrix(
        Vector(1, 0, 0, 0),
        Vector(0, 1, 0, 0),
        Vector(0, 0, 1, 0),
        Vector((x - m_startX) * m_scale, (y - m_startY) * m_scale, 0, 1));
}
