
#ifndef __COLOR_H
#define __COLOR_H

#include <ostream>

/**
    @file Color.h
    @brief Declares the Color class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/**
    @brief Defines a Color consisting of R, G, B and A components.
*/
class Color {
private:
    /** @brief The data array containing the R, G, B and A values. */
    float m_data[4];

    /** @brief Initializes the components to the specified values. */
    void initialize(float r, float g, float b);

    /** @brief Initializes the components to the specified values. */
    void initialize(float r, float g, float b, float a);

public:
    /** @brief The R coordinate index in the array representing this Color. */
    static const int R;
    /** @brief The G coordinate index in the array representing this Color. */
    static const int G;
    /** @brief The B coordinate index in the array representing this Color. */
    static const int B;
    /** @brief The A coordinate index in the array representing this Color. */
    static const int A;

    /** @brief Default constructor, initializes R, G and B components 
        to 0 and initializes A to 1. */
    Color();
    /** @brief Initializes the R, G and B components to the specified 
        values and initializes A to 1. */
    Color(float r, float g, float b);
    /** @brief Initializes the components to the specified values. */
    Color(float r, float g, float b, float a);

    /** @brief Mutable accessor for a Color component. */
    float& operator[](int which);

    /** @brief Immutable accessor for a Color component. */
    float operator[](int which) const;

    /** @brief Retrieve the color as an array of floats */
    const float* toArray() const;

    /**
        @brief Insertion operator for this Color into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Color& col);
};

#endif // __COLOR_H
