
#include <assert.h>
#include <math.h>

#include "Const.h"

#include "Quaternion.h"

using namespace std;

const int Quaternion::W = 0;
const int Quaternion::X = 1;
const int Quaternion::Y = 2;
const int Quaternion::Z = 3;

Quaternion::Quaternion()
{
    initialize(0.0, 0.0, 0.0, 0.0);
}

Quaternion::Quaternion(float w, float x, float y, float z)
{
    initialize(w, x, y, z);
}

Quaternion::Quaternion(float angle, const Vector& axis)
{
    m_data[W] = (float) cos(angle / 2);

    float _sin = (float) sin(angle / 2);
    m_data[X] = axis[Vector::X] * _sin;
    m_data[Y] = axis[Vector::Y] * _sin;
    m_data[Z] = axis[Vector::Z] * _sin;
}

void Quaternion::initialize(float w, float x, float y, float z)
{
    m_data[W] = w;
    m_data[X] = x;
    m_data[Y] = y;
    m_data[Z] = z;
}

float& Quaternion::operator[](int which)
{
    assert(which >= W && which <= Z);
    return m_data[which];
}

float Quaternion::operator[](int which) const
{
    assert(which >= W && which <= Z);
    return m_data[which];
}

const Quaternion Quaternion::normalize() const
{
    float _length = length();
    assert(_length > EPSILON);

    return Quaternion(
        m_data[W] / _length,
        m_data[X] / _length,
        m_data[Y] / _length,
        m_data[Z] / _length);
}

float Quaternion::length() const
{
    return (float) sqrt(m_data[W] * m_data[W] + 
        m_data[X] * m_data[X] + 
        m_data[Y] * m_data[Y] + 
        m_data[Z] * m_data[Z]);
}

const Matrix Quaternion::toMatrix() const
{
    return Matrix(
        Vector(
            1 - 2 * m_data[Y] * m_data[Y] - 2 * m_data[Z] * m_data[Z],
            2 * m_data[X] * m_data[Y] + 2 * m_data[W] * m_data[Z],
            2 * m_data[X] * m_data[Z] - 2 * m_data[W] * m_data[Y]),
        Vector(
            2 * m_data[X] * m_data[Y] - 2 * m_data[W] * m_data[Z],
            1 - 2 * m_data[X] * m_data[X] - 2 * m_data[Z] * m_data[Z],
            2 * m_data[Y] * m_data[Z] + 2 * m_data[W] * m_data[X]),
        Vector(
            2 * m_data[X] * m_data[Z] + 2 * m_data[W] * m_data[Y],
            2 * m_data[Y] * m_data[Z] - 2 * m_data[W] * m_data[X],
            1 - 2 * m_data[X] * m_data[X] - 2 * m_data[Y] * m_data[Y]));
}

ostream& operator<< (ostream& out, const Quaternion& quat)
{
    out << "[" << quat[Quaternion::W] << "," 
        << quat[Quaternion::X] << "," 
        << quat[Quaternion::Y] << "," 
        << quat[Quaternion::Z] << "]";
    return out;
}
