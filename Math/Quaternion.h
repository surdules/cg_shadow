
#ifndef __QUATERNION_H
#define __QUATERNION_H

#include <ostream>

#include "Vector.h"
#include "Matrix.h"

/**
    @file Quaternion.h
    @brief Declares the Quaternion class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/**
    @brief Defines a Quaternion consisting of W, X, Y and Z components.
*/
class Quaternion {
private:
    /** @brief The data array containing the W, X, Y and Z values. */
    float m_data[4];

private:
    /** @brief Initialize the components to the specified values. */
    void initialize(float w, float x, float y, float z);

public:
    /** @brief The W coordinate index in the array representing this Quaternion. */
    static const int W;
    /** @brief The X coordinate index in the array representing this Quaternion. */
    static const int X;
    /** @brief The Y coordinate index in the array representing this Quaternion. */
    static const int Y;
    /** @brief The Z coordinate index in the array representing this Quaternion. */
    static const int Z;

    /** @brief Default constructor, initializes all components to 0. */
    Quaternion();
    /** @brief Initializes the components to the specified values. */
    Quaternion(float w, float x, float y, float z);
    /**
        @brief Fills in the quaternion to represent the rotation
        determined by the specified angle around the specified axis.

        The quaternion is initialized as follows:
        <code>
        <ul>
            <li>w = cos(angle / 2)</li>
            <li>(x, y, z) = axis * sin(angle / 2)</li>
        </ul>
        </code>
    */
    Quaternion(float angle, const Vector& axis);

    /** @brief Mutable accessor for a Quaternion component. */
    float& operator[](int which);

    /** @brief Immutable accessor for a Quaternion component. */
    float operator[](int which) const;

    /**
        @brief Normalize the Quaternion: divide all components by the 
        Quaternion #length.
    */
    const Quaternion normalize() const;

    /** 
        @brief Returns the length of the Quaternion.

        The length is computed by taking the square root of the sum
        of the squares of the Quaternion components.
    */
    float length() const;

    /**
        @brief Returns the rotation matrix represented by this 
        quaternion.
    */
    const Matrix toMatrix() const;

    /**
        @brief Insertion operator for this Quaternion into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Quaternion& quat);
};

#endif // __QUATERNION_H
