
#include <assert.h>
#include <math.h>

#include "TransformerZoom.h"

TransformerZoom::TransformerZoom(int width, int height, float scale) {
    reshape(width, height);
    m_scale = scale;
}

void TransformerZoom::reshape(int width, int height) {
    assert((width > 0) && (height > 0));
}

void TransformerZoom::start(float x, float y) {
    m_startY = y;
}

const Matrix TransformerZoom::end(float x, float y) {
    return Matrix(
        Vector(1, 0, 0, 0),
        Vector(0, 1, 0, 0),
        Vector(0, 0, 1, 0),
        Vector(0, 0, (y - m_startY) * m_scale, 1));
}
