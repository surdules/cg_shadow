
#ifndef __CONST_H
#define __CONST_h

/**
    @file Const.h
    @brief Declares common constants.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/**
    @brief Small constant used to determine if a value is
    sufficiently close to a desired target.
*/
const float EPSILON = 1e-10f;

#endif // __CONST_H
