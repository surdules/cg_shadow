
#include <assert.h>

#include "Color.h"

using namespace std;

const int Color::R = 0;
const int Color::G = 1;
const int Color::B = 2;
const int Color::A = 3;

Color::Color()
{
    initialize(0.0, 0.0, 0.0);
}

Color::Color(float r, float g, float b)
{
    initialize(r, g, b);
}

Color::Color(float r, float g, float b, float a)
{
    initialize(r, g, b, a);
}

void Color::initialize(float r, float g, float b)
{
    initialize(r, g, b, 1.0);
}

void Color::initialize(float r, float g, float b, float a)
{
    m_data[R] = r;
    m_data[G] = g;
    m_data[B] = b;
    m_data[A] = a;
}

float& Color::operator[](int which)
{
    assert(which >= R && which <= A);
    return m_data[which];
}

float Color::operator[](int which) const
{
    assert(which >= R && which <= A);
    return m_data[which];
}

const float* Color::toArray() const
{
    return &m_data[0];
}

ostream& operator<< (ostream& out, const Color& col)
{
    out << "[" << 
        col.m_data[Color::R] << "," << 
        col.m_data[Color::G] << "," << 
        col.m_data[Color::B] << "," << 
        col.m_data[Color::A] << "]";
    return out;
}
