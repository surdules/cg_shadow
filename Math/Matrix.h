
#ifndef __MATRIX_H
#define __MATRIX_H

#include <ostream>

#include "Vector.h"

/**
    @file Matrix.h
    @brief Declares the Matrix class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/**
    @brief Defines a column-major 4x4 Matrix.
*/
class Matrix {
private:
    /** 
        @brief The data array containing the Matrix values, stored
        in column-major format:

        <pre>
        a0 a4 a8  a12
        a1 a5 a9  a13
        a2 a6 a10 a14
        a3 a7 a11 a15
        </pre>
    */
    float m_data[16];

private:
    /** 
        @brief Compute the determinant of the Matrix with the given 
        row and column removed.
    */
    float determinant(int row, int col) const;

    /** @brief Initialize the components to the specified values. */
    void initialize(const float* const data);

    /** @brief Initialize each column of the Matrix with one of the given vectors. */
    void initialize(const Vector& col1, const Vector& col2, 
        const Vector& col3, const Vector& col4);

public:
    /** @brief Constant Matrix representing the identity transformation. */
    static const Matrix IDENTITY;

    /** @brief Default constructor, initializes the Matrix to identity. */
    Matrix();
    /** @brief Initializes the components to the specified values. */
    Matrix(const float* const data);
    /** 
        @brief Initialize each column of the Matrix with one of the given vectors
        (the last column is initialized to (0,0,0,1).
    */
    Matrix(const Vector& col1, const Vector& col2, const Vector& col3);
    /** @brief Initialize each column of the Matrix with one of the given vectors. */
    Matrix(const Vector& col1, const Vector& col2, const Vector& col3, const Vector& col4);

    /** @brief Mutable accessor for a Matrix component. */
    float& operator()(int row, int col);
    /** @brief Immutable accessor for a Matrix component. */
    float operator()(int row, int col) const;

    /** @brief Accessor for the raw Matrix data. */
    const float* toArray() const;

    /** @brief Invert the Matrix. */
    const Matrix invert() const;

    /** @brief Transpose the Matrix. */
    const Matrix transpose() const;

    /** @brief Compute the determinant of the Matrix. */
    float determinant() const;

    /** @brief Multiply this Matrix on the right with another Matrix. */
    const Matrix multiplyRight(const Matrix& matrix) const;

    /** @brief Multiply this Matrix on the left with another Matrix. */
    const Matrix multiplyLeft(const Matrix& matrix) const;

    /** @brief Multiply this Matrix on the right with a Vector. */
    const Vector multiplyRight(const Vector& vector) const;

    /**
        @brief Insertion operator for this Matrix into an output
        stream.
    */
    friend std::ostream& operator<< (std::ostream& out, const Matrix& matrix);
};

#endif // __MATRIX_H
