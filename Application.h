
#ifndef __APPLICATION_H
#define __APPLICATION_H

#include "Color.h"
#include "Vector.h"
#include "Matrix.h"

#include "Model.h"

/**
    @file Application.h
    @brief Declares the Application class.
    @author <A HREF="mailto:razvan.surdulescu@post.harvard.edu">Razvan Surdulescu</A>
    @version 1.0
    @date 2/15/2003
*/

/** @brief Defines the Application we are about to run. */
class Application {
private:
    /** @brief The color of the ambient light. */
    Color m_lightAmbientColor;
    /** @brief The color of the diffuse light. */
    Color m_lightDiffuseColor;
    /** @brief The position of the diffuse light. */
    Vector m_lightPosition;
    /** @brief The position of the eye. */
    Vector m_eyePosition;

    /** @brief The scene matrix. */
    Matrix m_sceneMatrix;
    /** @brief The light matrix. */
    Matrix m_lightMatrix;

    /** @brief The model to draw on the screen. */
    Model m_model;

private:
    /** @brief Draw the scene surrounding the Model. */
    void drawScene() const;

public:
    /** @brief Default constructor, initialize application defaults. */
    Application();
    
    /** 
        @brief Setup OpenGL state and load the Model.
    */
    void init(const char* modelFileName);

    /** @brief Retrieve the eye position. */
    const Vector& getEyePosition() const;

    /** @brief Retrieve the Matrix which transforms the scene. */
    const Matrix& getSceneMatrix() const;
    /** @brief Set the Matrix which transforms the scene. */
    void setSceneMatrix(const Matrix& sceneMatrix);

    /** @brief Retrieve the Matrix which transforms the model (Model#getMatrix). */
    const Matrix& getModelMatrix() const;
    /** @brief Set the Matrix which transforms the scene (Model#setMatrix). */
    void setModelMatrix(const Matrix& modelMatrix);

    /** @brief Retrieve the Matrix which transforms the light. */
    const Matrix& getLightMatrix() const;
    /** @brief Set the Matrix which transforms the light. */
    void setLightMatrix(const Matrix& lightMatrix);

    /** @brief Retrieve the Model drawn by this Application. */
    Model& getModel();

    /** @brief Draw the scene. */
    void draw() const;
};

#endif // __APPLICATION_H
